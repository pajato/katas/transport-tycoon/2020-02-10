package com.pajato.katas

import com.pajato.katas.TransportState.*


fun getLastDeliveryHour(packages: String): Int {
    var packageIndex = 0
    var deliveredCount = 0
    var currentClockTick = 0
    var transferredCount = 0

    val trucks = arrayOf(Truck(), Truck())
    val boat = Boat()
    fun deliverProductMaybe() {
        fun startDeliveryMaybe() {
            fun startTruckTripMaybe() {
                for (truck in trucks)
                    if (truck.isIdle() && packages.length > packageIndex) truck.deliver(packages[packageIndex++])
            }
            fun startBoatTripMaybe() { if (boat.isIdle()) boat.deliver('A') }

            if (packageIndex < packages.length) startTruckTripMaybe()
            if (transferredCount > 0) startBoatTripMaybe()
        }
        fun registerDelivery(vehicle: Vehicle) {
            deliveredCount++
            if (vehicle is Boat) transferredCount--
            vehicle.state = Returning
        }
        fun updateBoat() {
            if (boat.isIdle()) return
            boat.update()
            if (boat.state == Delivered) registerDelivery(boat)
        }
        fun updateTruck(truck: Truck) {
            fun transferProductMaybe() {
                fun registerTransfer() { transferredCount++ }

                when (truck.state) {
                    Delivered -> registerDelivery(truck)
                    Transferred -> registerTransfer()
                    else -> return
                }
            }

            if (truck.isIdle()) return
            truck.update()
            transferProductMaybe()
        }

        startDeliveryMaybe()
        currentClockTick++
        for (truck in trucks) updateTruck(truck)
        updateBoat()
    }

    while (packages.length > deliveredCount) deliverProductMaybe()
    return currentClockTick //lastDeliveryHour
}

enum class TransportState { Idle, Delivered, Delivering, Returning, Transferred, Transferring }

abstract class Vehicle {
    internal var product: Char = 'X'
    internal var state = Idle
    internal var timeOfCurrentTrip = -1
    internal var tripLength = 0

    abstract fun deliver(product: Char)
    abstract fun getDeliveredState(): TransportState

    internal fun update() {
        fun setStateMaybe() {
            state = when (timeOfCurrentTrip) {
                tripLength / 2 -> getDeliveredState()
                tripLength -> Idle
                else -> return
            }
        }

        timeOfCurrentTrip++
        setStateMaybe()
        if (state == Idle) timeOfCurrentTrip = 0
    }

    internal fun isIdle() = state == Idle

}

class Boat : Vehicle() {

    override fun deliver(product: Char) {
        this.product = product
        state = Delivering
        timeOfCurrentTrip = 0
        tripLength = 8
    }

    override fun getDeliveredState() = Delivered

}

class Truck : Vehicle() {

    override fun deliver(product: Char) {
        this.product = product
        state = if (product == 'A') Transferring else Delivering
        timeOfCurrentTrip = 0
        tripLength = if (product == 'A') 2 else 10
    }

    override fun getDeliveredState() = if (product == 'A') Transferred else Delivered

}
