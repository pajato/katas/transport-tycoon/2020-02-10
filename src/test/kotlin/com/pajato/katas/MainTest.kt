package com.pajato.katas

import org.junit.Test
import kotlin.test.assertEquals

class MainTest {

    @Test
    fun `when input is empty verify result is 0`() {
        val actual = getLastDeliveryHour("")
        assertEquals(0, actual)
    }

    @Test
    fun `when input is B verify result is 5`() {
        val actual = getLastDeliveryHour("B")
        assertEquals(5, actual)
    }

    @Test
    fun `when input is A verify result is 5`() {
        val actual = getLastDeliveryHour("A")
        assertEquals(5, actual)
    }

    @Test
    fun `when input is AB verify result is 5`() {
        val actual = getLastDeliveryHour("AB")
        assertEquals(5, actual)
    }

    @Test
    fun `when inout is BB verify result is 5`() {
        val actual = getLastDeliveryHour("BB")
        assertEquals(5, actual)
    }

    @Test
    fun `when inout is ABB verify result is 7`() {
        val actual = getLastDeliveryHour("ABB")
        assertEquals(7, actual)
    }

    @Test
    fun `when input is BAB verify result is 7`() {
        val actual = getLastDeliveryHour("BAB")
        assertEquals(7, actual)
    }

    @Test
    fun `when input is BBA verify result is 15`() {
        val actual = getLastDeliveryHour("BBA")
        assertEquals(15, actual)
    }

    @Test
    fun `when input is AABABBAB verify result is 29`() {
        val actual = getLastDeliveryHour("AABABBAB")
        assertEquals(29, actual)
    }

    @Test
    fun `when input is ABBBABAAABBB verify result is tbd`() {
        val actual = getLastDeliveryHour("ABBBABAAABBB")
        assertEquals(41, actual)
    }
}
