# Transport Tycoon Kata - 2020-02-10

## Overview/Analysis

This kata implements a variant of the Transport Tycoon DDD project
originally described at https://github.com/Softwarepark/exercises/ .

In this variant there is a simulated transportation system in which a
factory produces products (packages) that must be delivered to
destinations using transport vehicles that consist of two trucks and a
boat.

The two product destinations have labels A and B.  Each product
identifies it's destination using these labels.

The simulated system is modeled using a Kotlin top level function
getLastDeliveryHour(). This function is invoked with a set
of products to be delivered modeled by an input string. Each
character in the input string identifies the product destination, A or B. The
products are delivered according to the lexical order, left to right,
within the input string.

Deliveries to B are made by a truck and take five hours to deliver and
five hours for the truck to return to the factory.

Deliveries to A are made by a truck traveling for one hour and a boat
traveling for four hours. If the boat is unavailable, the product is
left at the port until the boat returns. The truck returns to the
factory once the product is left at the port. The boat returns to the
port when the product is delivered at A.

The initial task is to implement and test getLastDeliveryHour()
using the following data:

| Output | Input          |
| :----: | :---:          |
| 0      | ""             |
| 5      | "A"            |
| 5      | "B"            |
| 5      | "AB"           |
| 5      | "BB"           |
| 7      | "ABB"          |
| 7      | "BAB"          |
| 15     | "BBA"          |
| 29     | "AABABBAB"     |
| tbd    | "ABBBABAAABBB" |

Once the method is working with these input values, report the result
for the input: ABBBABAAABBB

## About the implementation

This Kotlin implementation uses a functional programming style. A traditional OOP
variant was started but abandoned when it became clear that using a functional
paradigm would be far more concise and equally efficient.

The code was edited using IntelliJ and Emacs. Gradle is the build tool which allows
for either command line or IDE build, test and coverage support.

The code throughout adheres to the "Clean Code" practices and principles advocated
by Robert "Uncle Bob" Martin. Specifically, all functions are no more than four
(executed) lines long (making each function easy to understand, minimizing
indentation and eliminating the need for comments), TDD was employed (with a 100%
code coverage result).
 
The function structure uses a modified "Stepdown Rule": embedded functions are forward
declared and implemented as Kotlin does not allow embedded functions to be referenced
before declaration and implementation. The IDE does allow for these functions to be
"folded" this making them unobtrusive while in the IDE.

## Time spent on this exercise

My estimate is that about 12 hours went into this task broken down as follows:

| Task | Time (hours) | Notes |
| :----: | :---: | --- |
| Meetup | 4 | Includes setup and capture (Gitlab and IDE), socializing, and mob session 
| Analysis | 2 | Reading handout closely, reviewing links and writing up the result
| OOP Implementation | 2 | Partial implementation abandoned when it just did not feel right
| Functional Implementation | 4 | Throughout, this solution did feel correct

## Final comments

This task was a blast but expectations could have been better set ahead of time.
Perhaps it might be preferable to continue a problem of this size and scope over
multiple meet-ups.

I was surprised that the functional solution seemed so right.  While I develop both
functional and OOP code regularly, OOP code has been dominant for about 30-40 years
or so.  